import {gql} from '@apollo/client';
export const Api = {
    movies: gql`
        {
            movies {
                id
                name
            }
        }
    `
}
