import React, {useEffect, useState} from 'react';
import './App.css';
import {Api} from "./Api";
import {useQuery} from '@apollo/client';

const App = () => {
    const { data: moviesData, loading: moviesLoad } = useQuery(Api.movies);
  return (
    <div className="App">
      {moviesData && moviesData.movies.map(movie => (
          <div key={movie.id}>
            <div>{movie.name}</div>
            <div>{movie.id}</div>
          </div>
      ))}
    </div>
  );
}

export default App;
