const expresss = require('express')
const {graphqlHTTP} = require('express-graphql')
const schema = require('../schema/schema')
const mongoose = require('mongoose');
const cors = require('cors')

const app = expresss()
const PORT = 3005
const BD = 'твоя бд'
mongoose.Promise = global.Promise;
mongoose.connect(`${BD}`, { useUnifiedTopology: true, useNewUrlParser: true })

app.use(cors());
app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))

const dbConnection = mongoose.connection

dbConnection.on('error', err => console.log(err))
dbConnection.once('open', err => console.log("База данных подключена"))

app.listen(PORT, err => {
    err ? console.log(err) : console.log('Started servers!')
})
